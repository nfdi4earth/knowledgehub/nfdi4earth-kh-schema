# SPDX-FileCopyrightText: 2023 Senckenberg Society for Nature Research, TU Dresden
#
# SPDX-License-Identifier: CC0-1.0

[build-system]
build-backend = 'setuptools.build_meta'
requires = ['setuptools >= 61.0', 'versioneer[toml]']

[project]
name = "nfdi4earth-kh-schema"
dynamic = ["version"]
description = "Python package for working with data in conformance with the NFDI4Earth Knowledge Hub schema"


readme = "README.md"
keywords = [
    "linkml",
    ]

authors = [
    { name = 'Jonas Grieb', email = 'jonas.grieb@senckenberg.de' },
    { name = 'Ralf Klammer', email = 'ralf.klammer@tu-dresden.de' },
]
maintainers = [
    { name = 'Jonas Grieb', email = 'jonas.grieb@senckenberg.de' },
    { name = 'Ralf Klammer', email = 'ralf.klammer@tu-dresden.de' },
]
license = { text = 'Apache-2.0' }

classifiers = [
    "Intended Audience :: Developers",
    "License :: OSI Approved :: European Union Public Licence 1.2 (EUPL 1.2)",
    "Operating System :: OS Independent",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3 :: Only",
    "Programming Language :: Python :: 3.8",
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: 3.10",
    "Typing :: Typed",
]

requires-python = '>= 3.8'
dependencies = [
    'linkml==1.6.11',
    'linkml-runtime==1.6.3',
    'rdflib==6.3.2'
]

[project.urls]
Homepage = 'https://git.rwth-aachen.de/jgrieb/nfdi4earth-kh-schema'
Documentation = "https://nfdi4earth.pages.rwth-aachen.de/knowledgehub/nfdi4earth-kh-schema/"
Source = "https://git.rwth-aachen.de/jgrieb/nfdi4earth-kh-schema"
Tracker = "https://git.rwth-aachen.de/jgrieb/nfdi4earth-kh-schema/issues/"


[project.optional-dependencies]
# NOTE: recursive dependencies work only for pip>=21.2"
docs = [
    "mkdocs",
    "mkdocs-mermaid2-plugin",
    "mkdocs-material",
]
dev = [
    "nfdi4earth-kh-schema[docs]",
    "build",
    "twine"
]


[tool.mypy]
ignore_missing_imports = true

[tool.setuptools]
zip-safe = false
license-files = ["LICENSES/*"]

[tool.setuptools.package-data]
n4e_kh_schema_py = [
    "py.typed",
    "n4eschema-linkml.yaml",
    "n4eschema-enums.yaml",
    "schema.json",
    "context.jsonld",
]

[tool.setuptools.packages.find]
namespaces = false
exclude = [
    'docs',
    'tests*',
    'examples'
]

[tool.pytest.ini_options]
addopts = '-v'

[tool.versioneer]
VCS = 'git'
style = 'pep440'
versionfile_source = 'n4e_kh_schema_py/_version.py'
versionfile_build = 'n4e_kh_schema_py/_version.py'
tag_prefix = 'v'
parentdir_prefix = 'nfdi4earth-kh-schema-'

[tool.isort]
profile = "black"
line_length = 79
src_paths = ["n4e_kh_schema_py"]
float_to_top = true
known_first_party = "n4e_kh_schema_py"

[tool.black]
line-length = 79
target-version = ['py39']
