# This script


import click
import json
from linkml_runtime import SchemaView
from linkml_runtime.linkml_model.meta import (
    PermissibleValue,
    PermissibleValueText,
)

linkml_schema = None


def post_process_context(ld_context_file, linkml_schema_file):
    with open(ld_context_file) as file:
        context_wrapper = json.loads(file.read())
        context = context_wrapper["@context"]

    # load the linkml schema
    with open(linkml_schema_file) as file:
        linkml_schema_view = SchemaView(linkml_schema_file)
        linkml_schema = linkml_schema_view.schema

    remove_defs = []
    for key, entry in context.items():
        if key in linkml_schema.slots:
            slot = linkml_schema.slots[key]
            if slot.multivalued:
                entry["@container"] = "@set"
            if slot.range == "uriorcurie":
                if "@type" in entry and entry["@type"] == "@id":
                    entry.pop("@type")
            if "@context" in entry:
                entry.pop("@context")
            if "@type" in entry and entry["@type"] == "xsd:boolean":
                entry.pop("@type")
                if len(entry) == 0:
                    remove_defs.append(key)
    for key in remove_defs:
        context.pop(key)

    for slot_name, slot in linkml_schema.slots.items():
        if slot_name not in context and slot.multivalued:
            slot_def = {"@id": slot.slot_uri, "@container": "@set"}
            context[slot_name] = slot_def

    print(json.dumps(context_wrapper))


@click.command()
@click.option(
    "--linkml_schema",
    required=True,
    type=click.Path(exists=True, dir_okay=False),
)
@click.argument(
    "ld_context_file", type=click.Path(exists=True, dir_okay=False)
)
def cli(ld_context_file, **kwargs):
    """
    Post process JSON Schema representation of a LinkML model
    The updated JSON schema is piped to stdout
    """
    post_process_context(ld_context_file, kwargs["linkml_schema"])


if __name__ == "__main__":
    cli()
