# This script


import click
import json
from linkml_runtime import SchemaView
from linkml_runtime.linkml_model.meta import (
    PermissibleValue,
    PermissibleValueText,
)

linkml_schema_view = None
linkml_schema = None


def extract_permissible_text(pv):
    if isinstance(pv, str):
        return pv
    if isinstance(pv, PermissibleValue):
        if pv.meaning:
            return {"id": pv.meaning}
        else:
            return pv.text
    if isinstance(pv, PermissibleValueText):
        return pv
    raise ValueError(f"Invalid permissible value in enum, value {pv}")


def handle_strings_as_literals_or_references(prop, prop_definition, slot_def):
    any_of_string = [
        {"$ref": "#/$defs/title_plain"},
        {"$ref": "#/$defs/title_literal"},
    ]
    if slot_def.range in linkml_schema.classes:
        target_cls = linkml_schema.classes[slot_def.range]
        if "KHMetaDataObject" in target_cls.mixins:
            prop_definition["cordra"] = {
                "type": {"handleReference": {"types": [target_cls.name]}}
            }
            if "AllowsIRIOrBnode" in target_cls.mixins:
                prop_definition_clone = {}
                for k in list(prop_definition.keys()):
                    v = prop_definition.pop(k)
                    prop_definition_clone[k] = v
                prop_definition["anyOf"] = [
                    {"$ref": "#/$defs/" + slot_def.range},
                    prop_definition_clone,
                ]
            for child_class in linkml_schema_view.class_children(
                target_cls.name
            ):
                prop_definition["cordra"]["type"]["handleReference"][
                    "types"
                ].append(child_class)
    else:
        prop_definition.pop("type")
        prop_definition["anyOf"] = any_of_string


def post_process_schema(json_schema_file, linkml_schema_file):
    with open(json_schema_file) as file:
        schema = json.loads(file.read())
    schema["$defs"]["title_literal"] = {
        "additionalProperties": False,
        "properties": {
            "@language": {"type": "string"},
            "@value": {"type": "string"},
        },
        "required": ["@value"],
        "type": "object",
    }
    schema["$defs"]["title_plain"] = {"type": "string"}

    # load the linkml schema
    with open(linkml_schema_file) as file:
        global linkml_schema_view
        linkml_schema_view = SchemaView(linkml_schema_file)
        enums = linkml_schema_view.all_enums()
        global linkml_schema
        linkml_schema = linkml_schema_view.schema

    # linkml json schema generator uses for enums the "textual values",
    # however we want to validate json-ld, therefore enums where available
    # should make use of the CURIES
    for cls_, cls_definition in schema["$defs"].items():
        if cls_ in enums:
            enum = enums[cls_]
            permissible_values_texts = list(
                map(
                    extract_permissible_text,
                    enum.permissible_values.values() or [],
                )
            )
            if permissible_values_texts:
                cls_definition["enum"] = permissible_values_texts
                cls_definition["type"] = ["string", "object"]

        # allow string properties to optionally act like RDF literals (e.g.
        # having a language qualifer, etc.)
        if "properties" in cls_definition:
            for prop, prop_definition in cls_definition["properties"].items():
                if prop in linkml_schema.slots and "type" in prop_definition:
                    slot_def = linkml_schema_view.induced_slot(prop, cls_)
                    if (
                        prop_definition["type"] == "string"
                        and "format" not in prop_definition
                    ):
                        handle_strings_as_literals_or_references(
                            prop, prop_definition, slot_def
                        )

                    elif prop_definition["type"] == "array":
                        items = prop_definition["items"]
                        if (
                            "type" in items
                            and items["type"] == "string"
                            and "format" not in prop_definition
                        ):
                            handle_strings_as_literals_or_references(
                                prop, items, slot_def
                            )
                            slot = linkml_schema.slots[prop]
                            if slot.required:
                                prop_definition["minItems"] = 1

    print(json.dumps(schema))


@click.command()
@click.option(
    "--linkml_schema",
    required=True,
    type=click.Path(exists=True, dir_okay=False),
)
@click.argument(
    "json_schema_file", type=click.Path(exists=True, dir_okay=False)
)
def cli(json_schema_file, **kwargs):
    """
    Post process JSON Schema representation of a LinkML model
    The updated JSON schema is piped to stdout
    """
    post_process_schema(json_schema_file, kwargs["linkml_schema"])


if __name__ == "__main__":
    cli()
