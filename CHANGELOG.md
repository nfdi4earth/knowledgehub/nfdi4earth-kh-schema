<!--
SPDX-FileCopyrightText: 2023 Senckenberg Society for Nature Research, TU Dresden

SPDX-License-Identifier: CC0-1.0
-->

# Changelog

## v0.1.0: Initial release
