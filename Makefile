# SPDX-FileCopyrightText: 2023 Senckenberg Society for Nature Research, TU Dresden
#
# SPDX-License-Identifier: CC0-1.0

clean: clean-build clean-pyc clean-test clean-venv ## remove all build, virtual environments, test, coverage and Python artifacts

clean-build: ## remove build artifacts
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +

clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

clean-test: ## remove test and coverage artifacts
	rm -fr .tox/
	rm -f .coverage
	rm -fr htmlcov/
	rm -fr .pytest_cache

clean-venv:  # remove the virtual environment
	rm -rf venv

quick-test: ## run tests quickly with the default Python
	python -m pytest

docs-build: ## generate mkdocs HTML documentation based on LinkML
	# Run the patch on the gen-markdown script which shows the class_uri / slot_uri
	# in the markdown page instead of the uri of the self-defined linkml class
	FILE_PATH=$$(python -c "import linkml; print(linkml.__file__)" | sed -En "s/__init__.py/generators\/markdowngen.py/p"); \
	if ( patch --forward $${FILE_PATH} -i linkml_markdowngenerator_custom.patch ) ; then : ;\
	 else echo "\nIgnored patch because gen-markdown file was already patched"; fi

	# Include the controlled vocabulary as an independent ontology but in a sub path
	gen-markdown --useuris --img -d docs/controlled-vocabulary n4e-controlled-vocabs.yaml

	gen-markdown --useuris --img -d docs n4eschema-linkml.yaml
	# Run the patch for LinkML's gen-yuml script which avoids duplicate arrows
	# in overview diagram for relationships.
	FILE_PATH=$$(python -c "import linkml; print(linkml.__file__)" | sed -En "s/__init__.py/generators\/yumlgen.py/p"); \
	if ( patch --forward $${FILE_PATH} -i linkml_yumlgenerator_custom.patch ) ; then : ;\
	 else echo "\nIgnored patch because gen-yuml file was already patched"; fi
	# Now we remove the GenericIRI class from the linkml schema only for
	# the generation of the overview diagram, because otherwise this diagram
	# would become very messy with all classes having links to GenericIRI
	# Make a copy in order not to modify the original schema
	cp n4eschema-linkml.yaml n4eschema-linkml-simplified.yaml
	sed -i 's/GenericIRI:/GenericIRIWasSimplifiedWithuriorcurieForOverview:/g' n4eschema-linkml-simplified.yaml
	sed -i 's/GenericIRI/uriorcurie/g' n4eschema-linkml-simplified.yaml
	gen-yuml -d docs/images --diagram-name n4e-schema-full -f svg n4eschema-linkml-simplified.yaml
	# Delete the copy
	rm n4eschema-linkml-simplified.yaml
	sed -i '17 a [![img](images/n4e-schema-full.svg)](images/n4e-schema-full.svg)' docs/index.md
	mkdocs build --site-dir public

rdf-build: ## generate the ontology in all common RDF serialization formats
	if [ ! -d "public" ]; then mkdir public; fi
	gen-owl --no-metaclasses --no-type-objects --assert-equivalent-classes --metadata-profile rdfs -f ttl n4eschema-linkml.yaml > public/ontology.owl.ttl
	gen-owl --no-metaclasses --no-type-objects --assert-equivalent-classes --metadata-profile rdfs -f xml n4eschema-linkml.yaml > public/ontology.owl.xml
	gen-owl --no-metaclasses --no-type-objects --assert-equivalent-classes --metadata-profile rdfs -f nt n4eschema-linkml.yaml > public/ontology.owl.nt
	gen-owl --no-metaclasses --no-type-objects --assert-equivalent-classes --metadata-profile rdfs -f json-ld n4eschema-linkml.yaml > public/ontology.owl.jsonld

	if [ ! -d "public/controlled-vocabulary" ]; then mkdir public/controlled-vocabulary; fi
	gen-owl --no-metaclasses --no-type-objects --assert-equivalent-classes --metadata-profile rdfs -f ttl n4e-controlled-vocabs.yaml > public/controlled-vocabulary/ontology.owl.ttl
	gen-owl --no-metaclasses --no-type-objects --assert-equivalent-classes --metadata-profile rdfs -f xml n4e-controlled-vocabs.yaml > public/controlled-vocabulary/ontology.owl.xml
	gen-owl --no-metaclasses --no-type-objects --assert-equivalent-classes --metadata-profile rdfs -f nt n4e-controlled-vocabs.yaml > public/controlled-vocabulary/ontology.owl.nt
	gen-owl --no-metaclasses --no-type-objects --assert-equivalent-classes --metadata-profile rdfs -f json-ld n4e-controlled-vocabs.yaml > public/controlled-vocabulary/ontology.owl.jsonld

docs-serve: docs-build ## compile the docs watching for changes
	mkdocs serve

release: dist ## package and upload a release
	twine upload dist/*

dist: clean-build clean-pyc clean-test generate-python generate-json-schema-kh generate-jsonld-context-kh## builds source and wheel package, requires
	python -m build
	ls -l dist

dev-install: clean-build clean-pyc clean-test ## https://github.com/yaml/pyyaml/issues/601
	pip install "cython<3.0.0" wheel && pip install pyyaml==5.4.1 --no-build-isolation
	python -m pip install -e .[dev]
	FILE_PATH=$$(python -c "import linkml; print(linkml.__file__)" | sed -En "s/__init__.py/generators\/pythongen.py/p"); \
	patch $${FILE_PATH} < linkml_generator_custom.patch

generate-python:
	cp n4eschema-linkml.yaml n4e_kh_schema_py/
	cp n4eschema-enums.yaml n4e_kh_schema_py/
	gen-python n4eschema-linkml.yaml > n4e_kh_schema_py/n4eschema.py

generate-json-schema-kh:
	# first generate json schema based on the default linkml generator
	gen-json-schema n4eschema-linkml.yaml > schema.json
	# then apply the custom modifications
	python scripts/jsonschemagen_postprocess.py --linkml_schema n4eschema-linkml.yaml schema.json > n4e_kh_schema_py/schema.json

generate-jsonld-context-kh:
	# first generate jsonld context document based on the default linkml generator
	gen-jsonld-context n4eschema-linkml.yaml > context.jsonld
	# then apply the custom modifications
	python scripts/jsonldcontextgen_postprocessing.py --linkml_schema n4eschema-linkml.yaml context.jsonld > n4e_kh_schema_py/context.jsonld

venv-install: clean
    # try to init virtualenv with `python`
    # OR init by `python3` if that failed
	python -m venv venv || python3 -m venv venv
	@echo "#################################"
	@echo "Activate the virtenv now by executing: . venv/bin/activate"
	@echo "#################################"

venv-init: venv-install
	venv/bin/python -m pip install -e .[dev]
	PYTHON_VERSION=$$(python -c 'import sys; print(".".join(map(str, sys.version_info[:2])))'); \
	patch venv/lib/python$${PYTHON_VERSION}/site-packages/linkml/generators/pythongen.py < linkml_generator_custom.patch
	cp n4eschema-linkml.yaml n4e_kh_schema_py/
	cp n4eschema-enums.yaml n4e_kh_schema_py/
	venv/bin/gen-python n4eschema-linkml.yaml > n4e_kh_schema_py/n4eschema.py
