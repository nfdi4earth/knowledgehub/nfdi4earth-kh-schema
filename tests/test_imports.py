# SPDX-FileCopyrightText: 2023 Senckenberg Society for Nature Research, TU Dresden
#
# SPDX-License-Identifier: Apache-2.0

"""Test file for imports."""


def test_package_import():
    """Test the import of the main package."""
    import n4e_kh_schema_py  # noqa: F401
