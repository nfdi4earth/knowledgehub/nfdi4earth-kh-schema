<!--
SPDX-FileCopyrightText: 2023 Senckenberg Society for Nature Research, TU Dresden

SPDX-License-Identifier: CC-BY-4.0
-->

# Nfdi4earth Knowledge Hub Schema

This repository contains the underlying metadata schema of the NFDI4Earth Knowledge Hub.

The schema is created with [LinkML](https://linkml.io/). Out of the LinkML definition of the schema, we generate several serializations:

- Python bindings to interact with the schema, the generated code is published in the form of a Python package.
- Human-readable documentation of the schema with mkdocs

## Python

### Installation

This package is currently not registered at the [PyPI](https://pypi.org/) registry, but for each release we publish a package via the registry of this Gitlab project.

To use this package in another project install it via:

```
pip install --extra-index-url https://git.rwth-aachen.de/api/v4/projects/85620/packages/pypi/simple nfdi4earth-kh-schema
```

### Development installation

Clone this repository. It is recommended to run it in a Python virtual environment. As a shortcut you can run:

```bash
make venv-init # installs a virtual env and installs this package in development mode.
. venv/bin/activate # activates the virtual env
```

Alternatively, you can install the virtual environment by yourself:

```bash
python -m venv venv
. venv/bin/activate
make dev-install # installs this package in development mode.
```

Note: we are patching the functionality of LinkML in order to accept entities with blank nodes instead of URL identifiers for certain classes (TODO: expand on this)

Finally generate the Python classes from the LinkML definition:

```bash
make generate-python
```

**Important**: After every change in the `n4eschema-linkml.yaml` file, you need to run `make generate-python` (make sure that your virtual environment is activated, see above) again, in order to reflect the updated schema in the Python package! 

To update `schema.json`, run `make generate-json-schema-kh`. Alternatively, run `make dist` to update the Python package, `schema.json`, and `context.jsonld`.



[source code]: https://git.rwth-aachen.de/nfdi4earth/knowledgehub/nfdi4earth-kh-schema
[docs]: https://nfdi4earth.pages.rwth-aachen.de/knowledgehub/nfdi4earth-kh-schema/

## Generation of docs

The functionality of LinkML is used to automatically generate a human-readable documentation of the KH schema, which is based on [mkdocs](https://www.mkdocs.org/).

The generated docs are currently hosted at https://nfdi4earth.pages.rwth-aachen.de/knowledgehub/nfdi4earth-kh-schema/ - see the `.gitlab-ci.yml` file for the workflow.

### Locally build the docs

To test the generated documentation locally, run

```bash
# this initializes a Python virtual environment in the folder "venv" and installs the dependencies there
make venv-init
# activate the virtual environment
source venv/bin/activate
# build the docs based on the LinkML schema
make docs-build
# now try out via localhost
mkdocs serve
# access the static pages via your browser under the displayed url, usually http://127.0.0.1:8000/
```

The docs are currently making use of the `material` [theme for mkdocs](https://squidfunk.github.io/mkdocs-material/). This and other configs can be changed in the `mkdocs.yml`.

## Build and release

A new package is published in gitlab for every release, for this do:

```bash
git tag -a v0.4.x -m "describe changes..."
git push origin --tags
```

## Technical note

This package has been generated from the template
<https://git.rwth-aachen.de/nfdi4earth/architecture/python-package-template> (NOTE: several changes have been made to this package afterwards so it might be out of sync with the template).

See the template repository for instructions on how to update the skeleton for
this package.

## License information

Copyright © 2023 Senckenberg Society for Nature Research, TU Dresden

Licensed under the Apache-2.0

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the Apache-2.0 license for more details.

### License management

License management is handled with [`reuse`](https://reuse.readthedocs.io/).

Contributors: Jonas Grieb, Ralf Klammer, Daniel Nüst
