# SPDX-FileCopyrightText: 2023 Senckenberg Society for Nature Research, TU Dresden
#
# SPDX-License-Identifier: CC0-1.0

"""Setup script for the nfdi4earth-kh-schema package."""
import versioneer
from setuptools import setup

setup(
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
)
