# SPDX-FileCopyrightText: 2023 Senckenberg Society for Nature Research, TU Dresden
#
# SPDX-License-Identifier: Apache-2.0

"""NFDI4Earth KH Metadata Schema

Python package for working with data in conformance with the NFDI4Earth Knowledge Hub schema
"""

from __future__ import annotations

from . import _version

__version__ = _version.get_versions()["version"]

__author__ = "Jonas Grieb, Ralf Klammer"
__copyright__ = "2023 Senckenberg Society for Nature Research, TU Dresden"
__credits__ = [
    "Jonas Grieb",
    "Ralf Klammer",
]
__license__ = "EUPL-1.2"

__maintainer__ = "Jonas Grieb, Ralf Klammer"
__email__ = "jonas.grieb@senckenberg.de, ralf.klammer@tu-dresden.de"

__status__ = "Pre-Alpha"
